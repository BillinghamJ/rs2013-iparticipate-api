<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

function resource($prefix, $name, array $actions)
{
	$actionTypes = array(
		'index' => array('get', ''),
		'count' => array('get', '/count'),
		'search' => array('get', '/search'),
		'create' => array('post', ''),
		'show' => array('get', '/{id}'),
		'update' => array('put', '/{id}'),
		'destroy' => array('delete', '/{id}'));

	$classPrefix = ($prefix === NULL) ? '' : (Str::studly($prefix) . '\\');
	$className = Str::studly($name);
	$prefix = ($prefix === NULL) ? '' : ($prefix . '/');

	foreach ($actions as $action)
	{
		$actionType = $actionTypes[$action];
		$method = $actionType[0];
		$path = "{$prefix}{$name}{$actionType[1]}.json";
		$class = "{$classPrefix}{$className}Controller@{$action}";

		Route::$method($path, $class);
	}
}

Route::pattern('id', '[0-9]+');

resource(NULL, 'identities', array('index', 'count', 'create', 'show', 'update', 'destroy'));
