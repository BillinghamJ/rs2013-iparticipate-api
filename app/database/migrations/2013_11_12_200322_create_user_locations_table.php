<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('identity_id')->unsigned();
			$table->string('type');
			$table->float('latitude');
			$table->float('longitude');
			$table->timestamps();

			$table->foreign('identity_id')
				->references('id')
				->on('identities')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_locations', function($table)
		{
			$table->dropForeign('user_locations_identity_id_foreign');
		});

		Schema::drop('user_locations');
	}

}
