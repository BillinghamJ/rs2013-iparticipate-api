<?php

use LaravelBook\Ardent\Ardent;

class UserLocation extends Ardent
{
	protected $fillable = array('identity_id', 'type', 'latitude', 'longitude');

	public static $relationsData = array(
		'identity' => array(self::BELONGS_TO, 'Identity', 'table' => 'identities'));

	public static $rules = array(
		'identity_id' => 'required|exists:identities,id',
		'type' => 'required|in:identity_create',
		'latitude' => 'required|numeric|between:-90,90',
		'longitude' => 'required|numeric|between:-180,180');

	public function getIdentityAttribute()
	{
		return $this->identity()->getResults();
	}
}
