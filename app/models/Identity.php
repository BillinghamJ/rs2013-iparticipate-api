<?php

use LaravelBook\Ardent\Ardent;
use Illuminate\Auth\UserInterface;

class Identity extends Ardent implements UserInterface
{
	protected $fillable = array('name');
	protected $appends = array('token');

	public static $relationsData = array(
		'locations' => array(self::HAS_MANY, 'UserLocation', 'table' => 'user_locations'));

	public static $rules = array(
		'name' => 'max:50');

	public function getLocationsAttribute()
	{
		return $this->locations()->getResults()->toArray();
	}

	public function getTokenAttribute()
	{
		return Crypt::encrypt('Token' . $this->id);
	}

	public function setTokenAttribute()
	{
		// swallow this - nom
	}

	public function getAuthIdentifier()
	{
		return $this->id;
	}

	public function getAuthPassword()
	{
		return $this->token;
	}
}
