<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function ($request)
{
	//
});

App::after(function ($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function ()
{
	$authorization = Request::header('Authorization');

	if (!$authorization)
		App::abort(403, 'AuthorizationRequired');

	$authorization = explode(' ', $authorization, 2);

	if (count($authorization) != 2)
		App::abort(400, 'AuthorizationInvalid');

	switch ($authorization[0])
	{
		case 'Bearer':
			$token = $authorization[1];
			$decrypted = NULL;

			try
			{
				$decrypted = Crypt::decrypt($token);
			}
			catch (Exception $e) { }

			if (!$decrypted || substr($decrypted, 0, 5) !== 'Token')
				App::abort(403, 'TokenInvalid');

			// remove "Token" from start
			$id = substr($decrypted, 5);
			$identity = Identity::find($id);

			if (!$identity)
				App::abort(403, 'TokenInvalid');

			Auth::login($identity);
			break;

		default:
			App::abort(403, 'TokenUnsupported');
	}
});

Route::filter('auth_admin', function ()
{
	if (!Auth::user()->admin)
		App::abort(403, 'AccessDenied');
});
