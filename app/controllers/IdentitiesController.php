<?php

class IdentitiesController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('auth', array('only' => array('index', 'show', 'update', 'destroy')));
		$this->beforeFilter('auth_admin', array('only' => array('index')));
	}

	public function index()
	{
		return Identity::all();
	}

	public function count()
	{
		return array('count' => Identity::count());
	}

	public function create()
	{
		if (!Input::has('latitude')
			|| !Input::has('longitude'))
			App::abort(400, 'ParametersMissing');

		$lat = Input::get('latitude');
		$long = Input::get('longitude');

		if (!is_numeric($lat)
			|| !is_numeric($long))
			App::abort(400, 'ParametersInvalid');

		$lat = (float)$lat;
		$long = (float)$long;

		if ($lat < -90 || $lat > 90
			|| $long < -180 || $long > 180)
			App::abort(400, 'ParametersInvalid');

		$identity = new Identity;
		$identity->fill(Input::json()->all());

		if (!$identity->save())
			App::abort(400, 'DataInvalid');

		UserLocation::create(array(
			'identity_id' => $identity->id,
			'type' => 'identity_create',
			'latitude' => $lat,
			'longitude' => $long));

		return $identity;
	}

	public function show($id)
	{
		return $this->retrieveIdentity($id);
	}

	public function update($id)
	{
		$identity = $this->retrieveIdentity($id);
		$identity->fill(Input::json()->all());

		// admin has mass assignment protection
		if (Auth::user()->admin && Input::has('admin'))
			$identity->admin = Input::get('admin');

		if (!$identity->save())
			App::abort(400, 'DataInvalid');

		return $identity;
	}

	public function destroy($id)
	{
		$this->retrieveIdentity($id)->delete();
	}

	private function retrieveIdentity($id)
	{
		$identity = Identity::find($id);
		$current_identity = Auth::user();

		if (!$identity)
			App::abort(404, 'NotFound');

		if (!$current_identity->admin
			&& $identity != $current_identity)
			App::abort(403, 'AccessDenied');

		return $identity;
	}
}
